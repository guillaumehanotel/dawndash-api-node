import config from "../configuration";
import "reflect-metadata";
import { createConnection } from "typeorm";

export default async () => {
  try {
    await createConnection({
      type: "mysql",
      host: config.database.host,
      port: config.database.port,
      username: config.database.username,
      password: config.database.password,
      database: config.database.name,
      entities: ["src/models/*.ts"],
      synchronize: true,
      logging: true,
    });
  } catch (e) {
    console.log(e);
  }
};
