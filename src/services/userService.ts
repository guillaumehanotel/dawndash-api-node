import { Request } from "express";
import { getRepository, getManager } from "typeorm";
import { User } from "~/models/user";
import { GoogleProfile } from "~/models/googleProfile";
import authService from "~/services/authService";
import userService from "~/services/userService";
import { Tab } from "~/models/tab";
import { Bookmark } from "~/models/bookmark";
import { Setting } from "~/models/setting";

export default {
  async getUserFromRequest(req: Request): Promise<User | undefined> {
    const googleProfile = await authService.getGoogleProfileFromRequest(req);
    if (googleProfile && googleProfile.googleUid) {
      return await userService.getUserByGoogleUid(googleProfile.googleUid);
    }
  },
  async getUserByGoogleUid(googleUid: string): Promise<User | undefined> {
    return await getRepository(User)
      .createQueryBuilder("user")
      .leftJoinAndSelect("user.setting", "setting")
      .where("user.google_uid = :googleUid", { googleUid: googleUid })
      .getOne();
  },
  async createUserFromGoogleProfile(googleProfile: GoogleProfile, setting: Setting): Promise<User> {
    const user = new User();
    user.firstname = googleProfile.firstname;
    user.lastname = googleProfile.lastname;
    user.fullname = googleProfile.fullname;
    user.email = googleProfile.email;
    user.avatarUrl = googleProfile.avatarUrl;
    user.googleUid = googleProfile.googleUid;
    user.setting = setting;
    await getRepository(User).save(user);
    return user;
  },
  async createDefaultTabAndBookmarkForUser(user: User): Promise<void> {
    const tab = new Tab();
    tab.orderPosition = 1;
    tab.title = "Default";
    tab.user = user;
    await getManager().save(tab);

    const bookmark = new Bookmark();
    bookmark.isLink = true;
    bookmark.orderPosition = 1;
    bookmark.parentBookmarkId = null;
    bookmark.title = "Google";
    bookmark.url = "https://www.google.fr/";
    bookmark.thumbnailUrl = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_160x56dp.png";
    bookmark.tab = tab;
    bookmark.user = user;
    await getManager().save(bookmark);
  },
  async createDefaultSettings(): Promise<Setting> {
    const setting = new Setting();
    setting.backgroundImageUrl = "https://storage.cloud.google.com/dawndash-273809/vivaldi-gradient-wallpaper.jpg";
    setting.openInNewTab = false;
    setting.showBookmarkTitle = true;
    setting.bookmarkWidth = 250;
    return await getManager().save(setting);
  },
};
