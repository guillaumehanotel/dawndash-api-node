import { Request } from "express";
import config from "~/configuration";
import { LoginTicket, OAuth2Client } from "google-auth-library";
import { GoogleProfile } from "~/models/googleProfile";
import helper from "~/controllers/helper";
const client = new OAuth2Client(config.google.clientId);

export default {
  async getGoogleLoginTicketFromRequest(req: Request): Promise<LoginTicket | void> {
    const tokenId = helper.getTokenFromRequest(req);
    if (tokenId) {
      return await client.verifyIdToken({
        idToken: tokenId,
        audience: config.google.clientId,
      });
    } else {
      throw new Error("No 'Authorization' header provided or Invalid Token");
    }
  },

  async getGoogleProfileFromRequest(req: Request): Promise<GoogleProfile | void> {
    const loginTicket = await this.getGoogleLoginTicketFromRequest(req);
    if (loginTicket && loginTicket.getPayload()) {
      return new GoogleProfile(loginTicket);
    }
  },
};
