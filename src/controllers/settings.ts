import { Request, Response } from "express";
import { Setting } from "~/models/setting";
import { getRepository } from "typeorm";

export default {
  async updateSetting(req: Request, res: Response) {
    try {
      const { body } = req;
      const updatedSettings = {
        backgroundImageUrl: body.backgroundImageUrl,
        bookmarkWidth: body.bookmarkWidth,
        showBookmarkTitle: body.showBookmarkTitle,
        openInNewTab: body.openInNewTab,
      };
      await getRepository(Setting).update(req.params.settingsId, updatedSettings);
      return res.sendStatus(200);
    } catch (err) {
      console.error(err);
      res.status(500).json({
        error: err.message,
      });
    }
  },
};
