import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Tab } from "~/models/tab";
import userService from "~/services/userService";
import { User } from "~/models/user";

export default {
  async getTabsAndBookmarks(req: Request, res: Response) {
    try {
      const user = (await userService.getUserFromRequest(req)) as User;
      const tabsAndBookmarksByUser = await getRepository(Tab)
        .createQueryBuilder("tab")
        .leftJoinAndSelect("tab.bookmarks", "bookmark")
        .where("tab.user_id = :userId", { userId: user.id })
        .getMany();
      return res.json({
        tabs: tabsAndBookmarksByUser,
      });
    } catch (err) {
      res.status(500).json({
        error: err.message,
      });
    }
  },
  async createTab(req: Request, res: Response) {
    try {
      const { body } = req;
      const user = (await userService.getUserFromRequest(req)) as User;
      const tab = new Tab();
      tab.user = user;
      tab.title = body.title;
      tab.orderPosition = body.orderPosition;
      const result = await getRepository(Tab).save(tab);
      return res.status(201).json(result);
    } catch (err) {
      console.error(err);
      res.status(500).json({
        error: err.message,
      });
    }
  },
  async updateTab(req: Request, res: Response) {
    try {
      const { body } = req;
      const updatedTab = {
        title: body.title,
      };
      await getRepository(Tab).update(req.params.tabId, updatedTab);
      res.sendStatus(200);
    } catch (err) {
      console.error(err);
      res.status(500).json({
        error: err.message,
      });
    }
  },
  async deleteTab(req: Request, res: Response) {
    try {
      await getRepository(Tab)
        .createQueryBuilder("tabs")
        .delete()
        .where("tabs.id = :id", { id: req.params.tabId })
        .execute();
      res.sendStatus(204);
    } catch (err) {
      console.error(err);
      res.status(500).json({
        error: err.message,
      });
    }
  },
};
