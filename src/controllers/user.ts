import { Request, Response } from "express";
import authService from "~/services/authService";
import userService from "~/services/userService";

export default {
  async getOrCreateUser(req: Request, res: Response) {
    const googleProfile = await authService.getGoogleProfileFromRequest(req);
    if (googleProfile && googleProfile.googleUid) {
      let user = await userService.getUserByGoogleUid(googleProfile.googleUid);
      if (user === undefined) {
        const setting = await userService.createDefaultSettings();
        user = await userService.createUserFromGoogleProfile(googleProfile, setting);
        await userService.createDefaultTabAndBookmarkForUser(user);
        user = await userService.getUserByGoogleUid(googleProfile.googleUid);
      }
      res.status(200).json(user);
    } else {
      res.status(500).json({
        error: "Can't find Google Profile from token",
      });
    }
  },
};
