import { Request, Response } from "express";
import { Bookmark } from "../models/bookmark";
import { getManager, getRepository } from "typeorm";
import userService from "~/services/userService";
import { User } from "~/models/user";
import { Tab } from "~/models/tab";
import helper from "~/controllers/helper";

export default {
  async createBookmark(req: Request, res: Response) {
    try {
      const { body } = req;
      const user = (await userService.getUserFromRequest(req)) as User;
      const tab = ((await getManager().findOne(Tab, body.tabId)) as unknown) as Tab;
      const bookmark = new Bookmark();
      bookmark.title = body.title;
      bookmark.url = body.url;
      bookmark.thumbnailUrl = body.thumbnailUrl;
      bookmark.isLink = body.isLink;
      bookmark.orderPosition = body.orderPosition;
      bookmark.parentBookmarkId = body.parentBookmarkId;
      bookmark.user = user;
      bookmark.tab = tab;
      if (bookmark.isLink) {
        bookmark.thumbnailUrl = await helper.getThumbnailUrl(body.url);
      }
      const result = await getRepository(Bookmark).save(bookmark);
      return res.status(201).json(result);
    } catch (err) {
      console.error(err);
      res.status(500).json({
        error: err.message,
      });
    }
  },
  async updateBookmark(req: Request, res: Response) {
    try {
      const { body } = req;
      const updatedBookmark = {
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl,
        isLink: body.isLink,
        orderPosition: body.orderPosition,
      };
      await getRepository(Bookmark).update(req.params.bookmarkId, updatedBookmark);
      return res.sendStatus(200);
    } catch (err) {
      console.error(err);
      res.status(500).json({
        error: err.message,
      });
    }
  },
  async deleteBookmark(req: Request, res: Response) {
    try {
      await getRepository(Bookmark)
        .createQueryBuilder("bookmarks")
        .delete()
        .where("bookmarks.id = :id", { id: req.params.bookmarkId })
        .execute();
      res.sendStatus(204);
    } catch (err) {
      console.error(err);
      res.status(500).json({
        error: err.message,
      });
    }
  },
};
