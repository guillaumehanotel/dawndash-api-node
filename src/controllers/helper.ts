import { Request } from "express";
import { Storage } from "@google-cloud/storage";
import path from "path";
import captureWebsite from "capture-website";

const gc = new Storage({
  keyFilename: path.join(__dirname, "../../dawndash-273809-559685257dc3.json"),
  projectId: "dawndash-273809",
});

export default {
  getTokenFromRequest(req: Request) {
    const autorizationHeader = req.headers.authorization;
    if (autorizationHeader) {
      return autorizationHeader.split(" ")[1];
    }
  },
  async getThumbnailUrl(url: string) {
    const bucket = gc.bucket("dawndash-273809");
    const fileName = `${Date.now()}-screenshot.png`;
    const fileContent = await captureWebsite.buffer(url, {
      launchOptions: {
        args: ["--no-sandbox", "--disable-setuid-sandbox"],
      },
    });
    const file = bucket.file(fileName);
    await file.save(fileContent);
    return `https://storage.cloud.google.com/dawndash-273809/${fileName}`;
  },
};
