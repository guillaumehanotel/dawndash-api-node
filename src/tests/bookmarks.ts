import request from "supertest";
import { expect } from "chai";
import nock from "nock";

const ApiUrl = "http://localhost:3001/api";

describe("GET /api/bookmarks/", function() {
  it("Should send 'http://localhost:3001/api/bookmarks/'", function(done) {
    nock(ApiUrl)
      .get("/bookmarks/")
      .reply(200);
    request(ApiUrl)
      .get("/bookmarks/")
      .send({ userId: "1" })
      .end(function(err, res) {
        expect(res.status).to.equal(200);
        done();
      });
  });
});

describe("GET /api/bookmarks/1", function() {
  it("Should request 'http://localhost:3001/api/bookmarks/1' with userId = 1 ", async () => {
    nock(ApiUrl)
      .get("/bookmarks/1")
      .reply(200, {
        googleId: "asxcvfrtyhgfdsdfghjgfrer",
      });

    request(ApiUrl)
      .get("/bookmarks/1")
      .send({ userId: "1" })
      .end(function(err, res) {
        expect(res.body).to.have.property("googleId");
      });
  });
});

describe("POST /api/bookmarks/", function() {
  it("Should create a new bookmarks ", function(done) {
    nock(ApiUrl)
      .post("/bookmarks/")
      .reply(200);

    request(ApiUrl)
      .post("/bookmarks/")
      .send({
        isLink: "1",
        orderPosition: "1",
        parentBookmarkId: "3",
        title: "test",
        link: "test",
        tabId: "1",
        imagePath: "test",
        userId: "1",
      })
      .end(function(err, res) {
        expect(res.status).to.equal(200);
        done();
      });
  });
});

describe("PUT /api/bookmarks/", function() {
  it("Should update a bookmarks ", function(done) {
    nock(ApiUrl)
      .put("/bookmarks/1")
      .reply(200);
    request(ApiUrl)
      .put("/bookmarks/1")
      .send({
        isLink: "1",
        orderPosition: "1",
        parentBookmarkId: "3",
        title: "test",
        link: "test",
        tabId: "1",
        imagePath: "test",
        userId: "1",
      })
      .end(function(err, res) {
        expect(res.status).to.equal(200);
        done();
      });
  });
});

describe("DELETE /api/bookmarks/", function() {
  it("Should delete a bookmarks ", function(done) {
    nock(ApiUrl)
      .delete("/bookmarks/3")
      .reply(200);
    request(ApiUrl)
      .delete("/bookmarks/3")
      .end(function(err, res) {
        expect(res.status).to.equal(200);
        done();
      });
  });
});
