import request from "supertest";
import { expect } from "chai";
import nock from "nock";

/*eslint-disable */
const ApiUrl = "http://localhost:3000/api";
describe("Post /api/users/", function() {
  it("Should post a new user ", function(done) {
    nock(ApiUrl)
      .get("/users/")
      .reply(200, { google_id: "1azerty" });

    request(ApiUrl)
      .get("/users/")
      .send({ google_id: "1azerty" })
      .end(function(err, res) {
        expect(res.status).to.equal(200);
        done();
      });
  });
});
/*eslint-enable */
