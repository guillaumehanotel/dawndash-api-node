import request from "supertest";
import { expect } from "chai";
import nock from "nock";

const ApiUrl = "http://localhost:3000/api";
describe("GET /api/settings/", function() {
  it("Should request 'http://localhost:3000/api/settings/' ", function(done) {
    nock(ApiUrl)
      .get("/settings/")
      .reply(200, { userId: "1" });

    request(ApiUrl)
      .get("/settings/")
      .send({ userId: "1" })
      .end(function(err, res) {
        expect(res.status).to.equal(200);
        done();
      });
  });
});

describe("POST /api/settings/", function() {
  it("Should create a new settings ", function(done) {
    nock(ApiUrl)
      .post("/settings/")
      .reply(200);
    request(ApiUrl)
      .post("/settings/")
      .send({
        userId: "2",
        backgroundImage: "test",
        bookmarkWidth: "1",
        nbColumnMax: "1",
        showBookmarkTitle: "1",
      })
      .end(function(err, res) {
        expect(res.status).to.equal(200);
        done();
      });
  });
});

describe("PUT /api/settings/", function() {
  it("Should update a settings ", function(done) {
    nock(ApiUrl)
      .put("/settings/2")
      .reply(200);

    request(ApiUrl)
      .put("/settings/2")
      .send({
        backgroundImage: "test",
        bookmarkWidth: "15",
        nbColumnMax: "13",
        showBookmarkTitle: "0",
      })
      .end(function(err, res) {
        expect(res.status).to.equal(200);
        done();
      });
  });
});
