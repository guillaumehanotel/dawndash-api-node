import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import cors from "cors";
import * as Sentry from "@sentry/node";
import { RewriteFrames } from "@sentry/integrations";
import swaggerUi from "swagger-ui-express";
import routes from "./api";
import authService from "~/services/authService";
import swaggerDocument from "./swagger.json";

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace NodeJS {
    interface Global {
      __rootdir__: string;
    }
  }
}
global.__rootdir__ = __dirname || process.cwd();

export const getApp = () => {
  const app = express();
  Sentry.init({
    dsn: "https://eceaa9091c10401683327445d8fe099b@o375922.ingest.sentry.io/5202093",
    integrations: [
      new RewriteFrames({
        root: global.__rootdir__,
      }),
    ],
  });
  app.use(Sentry.Handlers.requestHandler() as express.RequestHandler);
  app.use(morgan("dev"));
  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use("/api-docs", swaggerUi.serve);
  app.use("/api-docs", swaggerUi.setup(swaggerDocument));
  app.use(async (req, res, next) => {
    try {
      if (process.env.NODE_ENV !== "test") {
        await authService.getGoogleLoginTicketFromRequest(req);
      }
      next();
    } catch (e) {
      res.status(401).send({
        error: e.message,
      });
    }
  });
  app.use("/api", routes());
  app.use("/api", (req, res) => res.redirect("/api-docs"));
  app.use(Sentry.Handlers.errorHandler() as express.ErrorRequestHandler);

  return app;
};
