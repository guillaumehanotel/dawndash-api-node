import { Router } from "express";
import user from "./routes/user";
import bookmarks from "./routes/bookmarks";
import settings from "./routes/settings";
import tabs from "./routes/tabs";

export default () => {
  const appRouter = Router();
  user(appRouter);
  bookmarks(appRouter);
  tabs(appRouter);
  settings(appRouter);

  return appRouter;
};
