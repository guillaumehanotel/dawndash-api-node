import { Router } from "express";
import settings from "~/controllers/settings";
const route = Router();

export default (app: Router) => {
  app.use("/settings", route);

  route.put("/:settingsId", settings.updateSetting);
};
