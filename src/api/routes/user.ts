import { Router } from "express";
import user from "~/controllers/user";
const route = Router();

export default (app: Router) => {
  app.use("/", route);
  route.get("/me", user.getOrCreateUser);
};
