import { Router } from "express";
import tabs from "~/controllers/tabs";
const route = Router();

export default (app: Router) => {
  app.use("/tabs", route);

  route.get("/", tabs.getTabsAndBookmarks);
  route.post("/", tabs.createTab);
  route.put("/:tabId", tabs.updateTab);
  route.delete("/:tabId", tabs.deleteTab);
};
