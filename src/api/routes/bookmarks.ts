import { Router } from "express";
import bookmarks from "~/controllers/bookmark";
const route = Router();

export default (app: Router) => {
  app.use("/bookmarks", route);

  route.post("/", bookmarks.createBookmark);
  route.put("/:bookmarkId", bookmarks.updateBookmark);
  route.delete("/:bookmarkId", bookmarks.deleteBookmark);
};
