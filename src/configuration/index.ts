export default {
  host: process.env.HOST || "localhost",
  port: process.env.PORT || "3001",
  database: {
    host: process.env.DB_HOST || "localhost",
    port: Number(process.env.DB_PORT) || 3306,
    username: process.env.DB_USERNAME || "root",
    password: process.env.DB_PASSWORD || "password",
    name: process.env.DB_NAME || "dawndash",
  },
  google: {
    clientId: process.env.GOOGLE_CLIENT_ID || "XXX-XXX.apps.googleusercontent.com",
  },
};
