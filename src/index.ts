import config from "~/configuration";
import { getApp } from "~/app";
import initDbConnection from "~/loaders/typeorm";

const startServer = () => {
  try {
    const app = getApp();
    initDbConnection().then(dbConnection => {
      app.set("dbInstance", dbConnection);
      app.listen(Number(config.port), config.host, () => {
        console.log(`server started at http://${config.host}:${config.port}`);
      });
    });
  } catch (error) {
    console.error(error);
  }
};

startServer();
