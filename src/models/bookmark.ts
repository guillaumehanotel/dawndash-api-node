import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from "typeorm";
import { User } from "~/models/user";
import { Tab } from "~/models/tab";

@Entity({ name: "bookmarks" })
export class Bookmark {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "boolean",
    name: "is_link",
  })
  isLink: boolean;

  @Column({
    type: "integer",
    name: "order_position",
  })
  orderPosition: number;

  @Column({
    type: "integer",
    name: "parent_bookmark_id",
    nullable: true,
  })
  parentBookmarkId: number | null;

  @Column("text")
  title: string;

  @Column({
    type: "text",
    nullable: true,
  })
  url: string;

  @Column({
    type: "text",
    name: "thumbnail_url",
    nullable: true,
  })
  thumbnailUrl: string;

  @ManyToOne(
    type => Tab,
    tab => tab.bookmarks,
    { onDelete: "CASCADE" },
  )
  @JoinColumn({ name: "tab_id" })
  tab: Tab;

  @ManyToOne(
    type => User,
    user => user.bookmarks,
    { onDelete: "CASCADE" },
  )
  @JoinColumn({ name: "user_id" })
  user: User;
}
