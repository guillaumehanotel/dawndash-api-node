import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "settings" })
export class Setting {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "text",
    name: "background_image_url",
  })
  backgroundImageUrl: string;

  @Column({
    type: "integer",
    name: "bookmark_width",
  })
  bookmarkWidth: number;

  @Column({
    type: "boolean",
    name: "show_bookmark_title",
  })
  showBookmarkTitle: boolean;

  @Column({
    type: "boolean",
    name: "open_in_new_tab",
  })
  openInNewTab: boolean;
}
