import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, JoinColumn } from "typeorm";
import { User } from "~/models/user";
import { Bookmark } from "~/models/bookmark";

@Entity({ name: "tabs" })
export class Tab {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("longtext")
  title: string;

  @Column({
    type: "integer",
    name: "order_position",
  })
  orderPosition: number;

  @OneToMany(
    type => Bookmark,
    bookmark => bookmark.tab,
    { onDelete: "CASCADE" },
  )
  bookmarks: Bookmark[];

  @ManyToOne(
    type => User,
    user => user.tabs,
  )
  @JoinColumn({ name: "user_id" })
  user: User;
}
