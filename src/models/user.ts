import { Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { Bookmark } from "~/models/bookmark";
import { Tab } from "~/models/tab";
import { Setting } from "~/models/setting";

@Entity({ name: "users" })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "text",
    name: "google_uid",
  })
  googleUid: string | null;

  @Column({
    type: "text",
    nullable: true,
  })
  fullname: string | null;

  @Column({
    type: "text",
    nullable: true,
  })
  firstname: string | null;

  @Column({
    type: "text",
    nullable: true,
  })
  lastname: string | null;

  @Column({
    name: "avatar_url",
    type: "text",
    nullable: true,
  })
  avatarUrl: string | null;

  @Column({
    type: "text",
    nullable: true,
  })
  email: string | null;

  @OneToMany(
    type => Bookmark,
    bookmark => bookmark.user,
    { onDelete: "CASCADE" },
  )
  bookmarks: Bookmark[];

  @OneToMany(
    type => Tab,
    tab => tab.user,
    { onDelete: "CASCADE" },
  )
  tabs: Tab[];

  @OneToOne(type => Setting)
  @JoinColumn({ name: "setting_id" })
  setting: Setting;
}
