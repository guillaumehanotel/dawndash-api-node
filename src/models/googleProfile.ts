import { LoginTicket } from "google-auth-library";

export class GoogleProfile {
  email: string | null;
  fullname: string | null;
  firstname: string | null;
  lastname: string | null;
  avatarUrl: string | null;
  googleUid: string | null;

  constructor(loginTicket: LoginTicket) {
    const payload = loginTicket.getPayload();
    this.email = payload ? (payload.email ? payload.email : null) : null;
    this.fullname = payload ? (payload.name ? payload.name : null) : null;
    this.firstname = payload ? (payload.given_name ? payload.given_name : null) : null;
    this.lastname = payload ? (payload.family_name ? payload.family_name : null) : null;
    this.avatarUrl = payload ? (payload.picture ? payload.picture : null) : null;
    this.googleUid = payload ? (payload.sub ? payload.sub : null) : null;
  }
}
