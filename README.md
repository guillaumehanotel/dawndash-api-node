## Dawndash - API

### Development

- Launch project :
	- `npm install`
	- `npm run dev`
- Launch local DB with Docker (optionnal) :
	- `docker run -it -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password mysql:5.7.21`
